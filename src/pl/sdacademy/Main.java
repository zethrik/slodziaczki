package pl.sdacademy;

import java.util.Scanner;

class Main {
    private static int convergekg(int n) {
        if (n > 0) {
            return convergekg(n / 1000);
        }
        return 1;
    }
    private static int przelicz (int n) {
        if (n > 0) {
            return przelicz(n * 100);
        }
        return 1;
    }

    public static void main(String[] args) {
        System.out.print("Podaj ile kilogramow chcesz przelicz na gramy: ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        System.out.println(System.in + "kilogramów to: " + convergekg(n) + "gramów.");

        System.out.println("podaj cm: ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        System.out.println("wynik wynosi: " + przelicz(n));
    }

}
